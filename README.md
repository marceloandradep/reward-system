# reward-system

## Solution

### Representation
In order to represent the inviting network I have used a graph structure. Each client is a node and each invite is an edge going from A to B. There are cases in which many clients would invite the same client A. In this situation the first invite is marked as "walkable = true" and the next invites are marked with "walkable = false". I am using this strategy so that I know which invite I just have to count and which I have to walk down the graph to calculate the deeper levels.

Ex. the input below generates a graph like the image.

![plain-graph.jpg](https://bitbucket.org/repo/njjgk7/images/4130629166-plain-graph.jpg)

In the image above the edge 3 -> 4 is represented by a solid line (which means this is walkable) while the edge 2 -> 4 with a dotted line (so this is not walkable).

### Score counting
For each client represented in a graph is assigned a score depending on the invites that he made and invites that his invitee made. There is also a factor that should be applied depending on the level of the invite. To represent this mechanism I have used an array where each element represents the number of invites and the index represents the level of the invites.

```
Ex. [2 1 1]
2 level one invites, 1 level two invite and 1 level three invite.
```

Using a graph traversal algorithm I should assign an array which represents the score for each node. See the imagem below.

![graph-with-scores.jpg](https://bitbucket.org/repo/njjgk7/images/1547206102-graph-with-scores.jpg)

#### Array shifting
Supose the graph below.

![array-shifting.jpg](https://bitbucket.org/repo/njjgk7/images/999341883-array-shifting.jpg)

The node 2 has a score of [2 1]. To calculate the score of node 1 (parent of node 2) we must shift node 2 array to the right and insert the value of 1 which indicates the parent's own score. There are some exceptions:
- For the case where a node doesn't have any children the score is nil;
- For the case where a node has a child which the score is nil his own score is [].

#### Array merging
To calculate the score of a parent node in terms of its children I have used an operation that I called "array merging". See the image below.

![array-merging.jpg](https://bitbucket.org/repo/njjgk7/images/1712821413-array-merging.jpg)

Looking at each counting operation separatedly (array shiftings) we have the creation of two arrays. To count the total for the parent node we should do a merging operation which consists in sum each corresponding element creating a new merged array. If one of the arrays is bigger than another we just concatenate the rest.

### Algorithm

#### Outter loop

```
Mark all nodes with a score of [].

While exists unvisited nodes
	Take the first unvisited node and calculate the score of its subjacent graph.
```	

#### Calculating the subjacent graph

```
Supose a node A
If A has children then

    For each child B do
        If the edge A -> B is walkable
            If B was visited already then do a "array merging" between B and A
            Else calculate the subjacent graph of B and then do the "array merging" with A
            
Else mark the score of A as nil.
```

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server-headless

## Usage

The application loads an input file on startup. To change the data that application will load change the contents of file input.txt in the resources folder.

### REST API

The application provides two REST endpoints. To querying for the scores ranking use:

```
GET http://localhost:3000/ranking
```

To insert new invites you could use:

```
PUT http://localhost:3000/invite/:from/:to
Ex.
http://localhost:3000/invite/5/6
will create an invitation from client 5 to client 6.
```